-----------------------------------------------------------------------------------------
--
-- game.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local widget = require( "widget" )
local transition = require( "transition" )
local math = require( "math" )
local physics = require( "physics" )
local audio = require('audio')

--------------------------------------------

local rows
local cols
local total_segments
local matrix_col_size
local matrix_row_size

local background -- pozadie

local puzzle  -- puzzle obrazok
local show_help_numbers  -- zobrazovanie cisiel ako pomocky

local image_fragments -- pole fragmentov

local pause_group  -- grupa na objekty zobrazene pri pauznuti
local result_group  -- grupa na objekty zobrazene pri vyrieseni puzzle
local time_out_group  -- grupa na objekty zobrazene pri vyprsani casu

local delay_box  -- blokovanie touch eventov

local result_time_value
local result_moves_value
local result_score_value

-- horny HID
local time_text
local moves_text
local level_text

local time
local moves
local level

local game_type
local image_path

local time_timer  -- pocitanie casu

--------------------------------------------

function check_left(fragment_position)
	local _position = fragment_position - 1
	if image_fragments[_position].image ~= nil then
		return nil
	else
		return _position
	end
end

function check_right(fragment_position)
	local _position = fragment_position + 1
	if image_fragments[_position].image ~= nil then
		return nil
	else
		return _position
	end
end

function check_up(fragment_position)
	local _position = fragment_position - cols
	if image_fragments[_position].image ~= nil then
		return nil
	else
		return _position
	end
end

function check_down(fragment_position)
	local _position = fragment_position + cols
	if image_fragments[_position].image ~= nil then
		return nil
	else
		return _position
	end
end

function format_time()
	local _minutes = math.floor(time / 60)
	local _seconds = time % 60

	if _seconds < 10 then
		_seconds = '0' .. _seconds
	end

	 return _minutes .. ':' .. _seconds
end

function update_hid()
	if time == 0 then
		time_text.text = '0:00'
	else
		time_text.text = format_time()
	end

	moves_text.text = moves
	level_text.text = level
end

function switch_items(current_item, empty_item, time, animate)
	if time == nil then time = 200 end
	if animate == nil then animate = true end

	if animate then
		sounds.play(sounds.sound.shuffle)
	end

	local _x_offset = (screenW - 286) / 2
	local _y_offset = (screenH - 286) / 2

	local current_item_x = _x_offset + ((current_item - 1) % cols) * matrix_col_size + (matrix_col_size * 0.5)
	local current_item_y = _y_offset + (math.floor((current_item - 1) / cols)) * matrix_row_size + (matrix_row_size * 0.5)

	local empty_item_x = _x_offset + ((empty_item - 1) % cols) * matrix_col_size + (matrix_col_size * 0.5)
	local empty_item_y = _y_offset + (math.floor((empty_item - 1) / cols)) * matrix_row_size + (matrix_row_size * 0.5)

	if animate then
		-- posuvame obrazok
		transition.to( image_fragments[current_item].image, {
			x = empty_item_x,
			y = empty_item_y,
			time = time
		} )

		-- a aj border
		transition.to( image_fragments[current_item].border, {
			x = empty_item_x,
			y = empty_item_y,
			time = time
		} )

		-- a aj cislo
		transition.to( image_fragments[current_item].number, {
			x = empty_item_x,
			y = empty_item_y,
			time = time
		} )
	else
		-- posunieme bez animacie
		image_fragments[current_item].image.x = empty_item_x
		image_fragments[current_item].image.y = empty_item_y

		image_fragments[current_item].border.x = empty_item_x
		image_fragments[current_item].border.y = empty_item_y

		image_fragments[current_item].number.x = empty_item_x
		image_fragments[current_item].number.y = empty_item_y
	end

	-- empty item posunieme bez animacie
	if image_fragments[empty_item].image ~= nil then
		image_fragments[empty_item].image.x = current_item_x
		image_fragments[empty_item].image.y = current_item_y

		-- a este aj konkretny image ma poziciu
		image_fragments[empty_item].image.current_position = current_item
	end

	if image_fragments[empty_item].border ~= nil then
		image_fragments[empty_item].border.x = current_item_x
		image_fragments[empty_item].border.y = current_item_y
	end

	if image_fragments[empty_item].number ~= nil then
		image_fragments[empty_item].number.x = current_item_x
		image_fragments[empty_item].number.y = current_item_y
	end

	-- updatneme pozicie danych prvkov
	image_fragments[current_item].position = empty_item
	image_fragments[empty_item].position = current_item

	-- a este aj konkretny image ma poziciu
	image_fragments[current_item].image.current_position = empty_item

	-- zamenime prvky
	image_fragments[current_item], image_fragments[empty_item] = image_fragments[empty_item], image_fragments[current_item]
end

function solved()
	-- utils.debug('game:solved')

	-- dalsie kliknutia blokujem
	delay_box.isHitTestable = true

	if settings.music then
		audio.fadeOut( {channel = sounds.channel.MUSIC_CHANNEL, time = 500} )
	end

	sounds.play(sounds.sound.success, sounds.channel.SFX_CHANNEL)
	sounds.vibrate()

	stop_timer()

	result_time_value.text = format_time()
	result_moves_value.text = moves

	local _category = R.arrays('category_label')[settings.selected_category]
	local _time_bonus = R.arrays('category_time_bonus')[settings.selected_difficulty]

	-- vypocitame score
	local bonus = (_time_bonus - time) * settings.selected_difficulty
	if bonus < 0 then bonus = 0 end

	-- maximum bodov
	local max_score = 500 * settings.selected_difficulty + 100 * level
	local min_score = settings.selected_difficulty * 25

	-- socre znizuje cas a pocet pohybov
	local score = max_score + bonus - (moves * settings.selected_difficulty)
	if score < 0 then score = 0 end

	-- pripocitame minimalne score
	score = score + min_score

	result_score_value.text = score

	-- nastavim pocet hviezdiciek
	local _stars = 0

	if time < _time_bonus then
		-- ok, budem riesit hviezdicky
		_stars = 1

		if time < _time_bonus - 0.15 * _time_bonus then
			_stars = 2
		end

		if time < _time_bonus - 0.25 * _time_bonus then
			_stars = 3
		end

		-- mam pocet hviezdiciek, este ich ulozim do settingov
		if settings['levels'][_category]['stars'][level] < _stars then 
			settings['levels'][_category]['stars'][level] = _stars
			utils.saveSettings(settings)
		end
	end

	-- odomkneme dalsi level
	if game_type ~= 'random-puzzle' then
		if settings['levels'][_category]['unlocked'] < level + 1 and level < settings['levels'][_category]['level_count'] then
			settings['levels'][_category]['unlocked'] = level + 1

			utils.saveSettings(settings)
		end
	end

	if settings.game_play_counter % 3 == 0 then
		if ads.isLoaded("interstitial") then
			ads.show( "interstitial", { x=0, y=0, testMode = false } )
		end
	end

	if not is_emulator then
		--utils.debug('is_emulator', is_emulator)

		-- submitneme score
		game_network.send_score(R.arrays('board_id')[platform_name][settings['selected_difficulty']], score)

		----------------------------------------
		-- incremental nie je zatial podporovane
		----------------------------------------

		-- -- odomknem achievements pre pocet hier
		-- for i = 1, 3 do
		-- 	game_network.unlock_achievement(R.arrays('achievements_play')[platform_name][settings['selected_difficulty']][i])
		-- end

		-- -- ak je cas do 5 minut, odomknem achievements
		-- if time < 300 then
		-- 	for i = 1, 3 do
		-- 		game_network.unlock_achievement(R.arrays('achievements_five_minutes')[platform_name][settings['selected_difficulty']][i])
		-- 	end
		-- end

		-- -- ak je cas do 1 minuty, odomknem achievements
		-- if time < 60 then
		-- 	for i = 1, 3 do
		-- 		game_network.unlock_achievement(R.arrays('achievements_one_minute')[platform_name][settings['selected_difficulty']][i])
		-- 	end
		-- end

		-- -- ak je na 3 hviezdicky
		-- if _stars == 3 then
		-- 	for i = 1, 3 do
		-- 		game_network.unlock_achievement(R.arrays('achievements_play_three_stars')[platform_name][settings['selected_difficulty']][i])
		-- 	end
		-- end
	end

	-- a do analytics posleme event
	ga:event('game', 'solved', 'selected_difficulty', settings['selected_difficulty'])
	ga:event('game', 'solved', 'score', score)
	ga:event('game', 'solved', 'time', time)

	delay_box.isHitTestable = true

	-- zobrazime moznost kliknut a zobrazit result
	timer.performWithDelay( 2000, show_results )
end

function show_results()
	-- povolujem kliknutia
	delay_box.isHitTestable = false

	-- zobrazime screen
	result_group.alpha = 1
end

function check_solution()
	-- vzdy ked kontrolujem riesenie, nastal pohyb
	moves = moves + 1
	update_hid()

	-- ak posledny item nie je prazdny, tak nemame riesenie
	if image_fragments[#image_fragments].image ~= nil then return false end

	local _solved = true

	-- kontrolujeme poradie itemov
	for i = 1, #image_fragments - 1 do
		if image_fragments[i].solved_position ~= i then
			_solved = false
		end
	end

	-- ak je puzzle vyriesene, zobrazime zhodnotenie
	if _solved then solved() end
end

function image_fragment_touch_handler( self, event )
	if event.phase ~= 'began' then return true end

	local _check_solution = false

	-- utils.debug('game:image_fragment_touch_handler', self.current_position)

	if (self.current_position - 1) % cols > 0 then
		-- tento fragment nie je uplne vlavo
		-- kontrolujeme prvok na lavo od neho
		-- utils.debug('game:image_fragment_touch_handler', 'check left')

		local _position_status = check_left(self.current_position)
		if _position_status ~= nil then
			-- vymenime prvky
			switch_items(self.current_position, _position_status)

			-- kontrolujeme stav riesenia
			check_solution()

			return true
		end
	end

	if self.current_position % cols > 0 then
		-- tento fragment nie je uplne vpravo
		-- kontrolujeme prvok na pravo od neho
		-- utils.debug('game:image_fragment_touch_handler', 'check right')

		local _position_status = check_right(self.current_position)
		if _position_status ~= nil then
			-- vymenime prvky
			switch_items(self.current_position, _position_status)

			-- kontrolujeme stav riesenia
			check_solution()

			return true
		end
	end

	if math.floor((self.current_position - 1) / rows) > 0 then
		-- tento fragment nie je uplne hore
		-- kontrolujeme prvok nad nim
		-- utils.debug('game:image_fragment_touch_handler', 'check up')

		local _position_status = check_up(self.current_position)
		if _position_status ~= nil then
			-- vymenime prvky
			switch_items(self.current_position, _position_status)

			-- kontrolujeme stav riesenia
			check_solution()

			return true
		end
	end

	if math.floor((self.current_position - 1) / rows) < (rows - 1) then
		-- tento fragment nie je uplne dole
		-- kontrolujeme prvok pod nim
		-- utils.debug('game:image_fragment_touch_handler', 'check down')

		local _position_status = check_down(self.current_position)
		if _position_status ~= nil then
			-- vymenime prvky
			switch_items(self.current_position, _position_status)

			-- kontrolujeme stav riesenia
			check_solution()

			return true
		end
	end

	return true
end

function get_image_path()
	-- utils.debug('game:get_image_path', game_type, image_path)

	local _category
	local _level
	local _type

	local _path

	if game_type == 'random-puzzle' then
		if image_path ~= nil then
			_path = image_path
		else
			-- nahodne vyberiem kategoriu
			_category = R.arrays('category_label')[math.random( #R.arrays('category_label') )]

			-- nahodne vyberiem item v kategorii
			_level = math.random(settings['levels'][_category]['unlocked'])

			_path = 'images/puzzle/' .. _category .. '/' .. R.arrays('puzzle_images')[_category][_level]

			-- este ulozime aktualny orazok
			image_path = _path
		end
	else
		_category = R.arrays('category_label')[settings.selected_category]
		_level = level

		_path = 'images/puzzle/' .. _category .. '/' .. R.arrays('puzzle_images')[_category][_level]
	end

	ga:event('game', 'get_image_path', game_type, _path)

	return _path
end

function build_playground(group)
	-- obrazok, ktory sa bude skladat
	local sheetWidth = 496
	local sheetHeight = 496
	local imageSheetOptions = {
		width = sheetWidth / cols,
		height = sheetHeight / rows,
		numFrames = total_segments,
		sheetContentWidth = sheetWidth,
		sheetContentHeight = sheetHeight
	}

	-- loadmene spravny obrazok podla levelu a kategorie
	local _path = get_image_path()

	local image_sheet = graphics.newImageSheet( _path, imageSheetOptions )

	local image_fragments_group = display.newGroup( )

	-- mame prazdne pole fragmentov
	image_fragments = {}

	local _x_offset = (screenW - 286) / 2
	local _y_offset = (screenH - 286) / 2

	-- border
	local border = display.newRect( halfW, halfH, 286, 286 )
	border:setFillColor( 0, 0, 0 )
	border:setStrokeColor( utils.color(241, 196, 15), 0.3 )
	border.strokeWidth = 1
	group:insert(border)

	for i = 1, total_segments do
		local _x = _x_offset + ((i - 1) % cols) * matrix_col_size + (matrix_col_size * 0.5)
		local _y = _y_offset + (math.floor((i - 1) / rows)) * matrix_row_size + (matrix_row_size * 0.5)

		local _image
		local _border
		local _number

		if i == total_segments then
			-- posledny fragment je prazdny
			_image = nil
			_border = nil
			_number = nil
		else
			_border = display.newRect( _x, _y, matrix_col_size, matrix_row_size)
			_border.strokeWidth = 1
			_border:setStrokeColor( 0, 0, 0 )
			_border:setFillColor( utils.color(241, 196, 15) )
			_border.alpha = 0.4
			image_fragments_group:insert( _border )

			-- standardny fragment
			_image = display.newImageRect(image_fragments_group, image_sheet, i, matrix_col_size - 2, matrix_row_size - 2 )
			_image.x = _x
			_image.y = _y

			_image.current_position = i

			utils.handlerAdd(_image, 'touch', image_fragment_touch_handler)

			image_fragments_group:insert( _image )

			-- help cislo, toto bude defaul vypnute
			_number = display.newEmbossedText( {
				text = i,
				fontSize = 20,
				font = 'Ubuntu',
				x = _x,
				y = _y,
			} )
			_number:setFillColor( 1, 1, 1 )
			_number.alpha = 0
			image_fragments_group:insert( _number )
		end

		image_fragments[i] = {
			position = i,
			solved_position = i,
			image = _image,
			border = _border,
			number = _number
		}
	end

	group:insert(image_fragments_group)
end

function remove_playground()
	for i = 1, #image_fragments do
		display.remove(image_fragments[i].number)
		display.remove(image_fragments[i].border)
		display.remove(image_fragments[i].image)
	end

	image_fragments = nil
end

function show_numbers_handler( self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	local _alpha

	if show_help_numbers then
		show_help_numbers = false
		_alpha = 0
	else
		show_help_numbers = true
		_alpha = 1
	end

	-- prejdem cez vsetky cisla a upravim im alpa hodnotu
	for i = 1, #image_fragments do
		if image_fragments[i].number ~= nil then
			image_fragments[i].number.alpha = _alpha
		end
	end

	ga:event('game', 'button', 'show_numbers_handler', show_help_numbers)

	return true
end

function is_solvable()
	local inversions = 0
	local empty_row = 0

	for i = 1, #image_fragments do
		if image_fragments[i].number == nil then
			-- poziciu prazdeho prvku pouzijem nizsie
			empty_row = math.floor((i - 1) / cols) + 1
		end

		for j = i, #image_fragments do
			if image_fragments[i].number ~= nil and image_fragments[j].number ~= nil then
				if image_fragments[i].solved_position > image_fragments[j].solved_position then
					inversions = inversions + 1
				end
			end
		end
	end

	if #image_fragments % 2 == 0 then
		-- pre parne puzzle musime pripocitat poziciu prazdneho itemu
		inversions = inversions + rows - empty_row
	end

	if inversions % 2 == 0 then
		return true
	else
		return false
	end
end

function shuffle()
	-- pomiesame obrazok
	for i = 1, #image_fragments do
		-- zvolime nahodnu poziciu, kam prvok presunieme
		_random = math.random(#image_fragments)

		-- ak su pozicie rovnake, tak nic nerobime, inak vymenim itemy
		if _random ~= i then
			if image_fragments[i].image ~= nil then
				switch_items(i, _random, nil, false)
			else
				switch_items(_random, i, nil, false)
			end
		end
	end

	-- skontrolujeme, ci ma riesenie
	-- http://www.sitepoint.com/randomizing-sliding-puzzle-tiles/
	if not is_solvable() then
		-- nema, takze vymenime itemy tak, aby malo
		if image_fragments[1].number ~= nil and image_fragments[2].number ~= nil then
			-- vymenime prve dva itemy
			switch_items(1, 2, nil, false)
		else
			-- vymenim posledne dva itemy
			switch_items(#image_fragments, #image_fragments - 1, nil, false)
		end
	end
end

function continue_handler(self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	--utils.debug('game:continue_handler')

	pause_group.alpha = 0
	timer.resume(time_timer)

	ga:event('game', 'button', 'continue_handler', time)

	return true
end

function pause_handler( self, event)
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	--utils.debug('game:pause_handler')

	pause_group.alpha = 1
	timer.pause(time_timer)

	ga:event('game', 'button', 'pause_handler', time)

	return true
end

function main_menu_handler( self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	--utils.debug('game:main_menu_handler')

	stop_timer()

	ga:event('game', 'button', 'main_menu_handler', time)

	composer.gotoScene( 'menu' )

	return true
end

function replay_handler( self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	stop_timer()

	ga:event('game', 'button', 'replay_handler', time)

	if game_type ~= 'random-puzzle' then
		-- ak to nie je random puzzle, tak vynulujem image path
		image_path = nil
	end

	composer.gotoScene( 'game', {
		params = {
			level = level,
		}
	})

	return true
end

function next_level_handler( self, event )
	if event.phase ~= 'began' then return true end

	-- ak je mozne hrat dalsi level
	if game_type == 'random-puzzle' then
		-- ideme na ine puzzle
		image_path = nil

		composer.gotoScene( 'game', {
			params = {
				level = level,
				game_type = game_type
			}
		})
	else
		local _category = R.arrays('category_label')[settings.selected_category]

		--utils.debug('game:next_level_handler', settings['levels'][_category]['unlocked'], level, settings['levels'][_category]['level_count'])

		if level < settings['levels'][_category]['unlocked'] then
			level = level + 1

			composer.gotoScene( 'game', {
				params = {
					level = level,
					game_type = game_type
				}
			})
		end
	end
end

function build_result_screen(sceneGroup)
	-- pause grupa
	result_group = display.newGroup()
	result_group.alpha = 0
	sceneGroup:insert( result_group )

	local box = display.newRect( 0, 0, screenW, screenH )
	box.anchorX = 0
	box.anchorY = 0
	box.alpha = 0.85
	box:setFillColor( 0, 0, 0 )
	result_group:insert( box )

	utils.handlerAdd(box, 'touch', function ( ... )
		return true
	end)

	local solved_label = display.newText( {
		text = R.strings('solved'),
		fontSize = 48,
		font = 'Ubuntu',
		x = halfW,
		y = 50,
	} )
	solved_label:setFillColor( 1, 1, 1 )
	result_group:insert(solved_label)

	local good_job_label = display.newText( {
		text = R.strings('good_job'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW,
		y = 86,
	} )
	good_job_label:setFillColor( 1, 1, 1 )
	result_group:insert(good_job_label)

	-- vysledky hry
	-- celkovy cas
	local time_icon = display.newImageRect( sceneGroup, 'images/time.png', 25, 28 )
	time_icon.anchorX = 1
	time_icon.x = halfW - 5
	time_icon.y = 140
	result_group:insert( time_icon )

	result_time_value = display.newText( {
		text = '',
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW + 5,
		y = 140,
	} )
	result_time_value.anchorX = 0
	result_time_value:setFillColor( 1, 1, 1 )
	result_group:insert(result_time_value)

	-- pocet pohybov
	local moves_icon = display.newImageRect( sceneGroup, 'images/moves.png', 23, 23 )
	moves_icon.anchorX = 1
	moves_icon.x = halfW - 5
	moves_icon.y = 180
	result_group:insert( moves_icon )

	result_moves_value = display.newText( {
		text = '',
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW + 5,
		y = 180,
	} )
	result_moves_value.anchorX = 0
	result_moves_value:setFillColor( 1, 1, 1 )
	result_group:insert(result_moves_value)

	-- score
	local score_label = display.newText( {
		text = R.strings('score'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 5,
		y = 220,
	} )
	score_label.anchorX = 1
	score_label:setFillColor( 1, 1, 1 )
	result_group:insert(score_label)

	result_score_value = display.newText( {
		text = '',
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW + 5,
		y = 220,
	} )
	result_score_value.anchorX = 0
	result_score_value:setFillColor( 1, 1, 1 )
	result_group:insert(result_score_value)

	local _category = R.arrays('category_label')[settings.selected_category]
	if level < settings['levels'][_category]['level_count'] then
		-- next level
		local play_icon = display.newImageRect( sceneGroup, 'images/continue.png', 15, 16 )
		play_icon.anchorX = 1
		play_icon.x = halfW - 40
		play_icon.y = screenH - 130
		result_group:insert(play_icon)

		local play = display.newText( {
			text = R.strings('play_next_level'),
			fontSize = 18,
			font = 'Ubuntu',
			x = halfW - 25,
			y = screenH - 130,
		} )
		play.anchorX = 0
		play:setFillColor( 1, 1, 1 )
		result_group:insert(play)

		local play_button = display.newRect( halfW + 25, screenH - 130, 180, 40 )
		play_button.alpha = 0
		play_button.isHitTestable = true
		result_group:insert( play_button )

		utils.handlerAdd(play_button, 'touch', next_level_handler)
	end 

	-- replay
	local menu_icon = display.newImageRect( sceneGroup, 'images/replay.png', 16, 15 )
	menu_icon.anchorX = 1
	menu_icon.x = halfW - 40
	menu_icon.y = screenH - 90
	result_group:insert(menu_icon)

	local menu = display.newText( {
		text = R.strings('replay'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 25,
		y = screenH - 90,
	} )
	menu.anchorX = 0
	menu:setFillColor( 1, 1, 1 )
	result_group:insert(menu)

	local menu_button = display.newRect( halfW + 15, screenH - 90, 160, 40 )
	menu_button.alpha = 0
	menu_button.isHitTestable = true
	result_group:insert( menu_button )

	utils.handlerAdd(menu_button, 'touch', replay_handler)

	-- main menu
	local menu_icon = display.newImageRect( sceneGroup, 'images/menu.png', 16, 16 )
	menu_icon.anchorX = 1
	menu_icon.x = halfW - 40
	menu_icon.y = screenH - 50
	result_group:insert(menu_icon)

	local menu = display.newText( {
		text = R.strings('main_menu'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 25,
		y = screenH - 50,
	} )
	menu.anchorX = 0
	menu:setFillColor( 1, 1, 1 )
	result_group:insert(menu)

	local menu_button = display.newRect( halfW + 15, screenH - 50, 160, 40 )
	menu_button.alpha = 0
	menu_button.isHitTestable = true
	result_group:insert( menu_button )

	utils.handlerAdd(menu_button, 'touch', main_menu_handler)
end

function build_pause_screen(sceneGroup)
	-- pause grupa
	pause_group = display.newGroup()
	pause_group.alpha = 0
	sceneGroup:insert( pause_group )

	local pause_bg = display.newImageRect( 'images/pause-bg.png', 286, 286 )
	pause_bg.x = halfW
	pause_bg.y = halfH
	pause_bg.alpha = 0.90
	pause_group:insert( pause_bg )

	local box = display.newRect( 0, 0, screenW, screenH )
	box.anchorX = 0
	box.anchorY = 0
	box.alpha = 0.90
	box:setFillColor( 0, 0, 0 )
	pause_group:insert( box )

	utils.handlerAdd(box, 'touch', function ( ... )
		return true
	end)

	local paused_label = display.newText( {
		text = R.strings('paused'),
		fontSize = 48,
		font = 'Ubuntu',
		x = halfW,
		y = halfH - 50,
	} )
	paused_label:setFillColor( 1, 1, 1 )
	pause_group:insert(paused_label)

	-- continue play
	local play_icon = display.newImageRect( sceneGroup, 'images/continue.png', 15, 16 )
	play_icon.anchorX = 1
	play_icon.x = halfW - 40
	play_icon.y = screenH - 90
	pause_group:insert(play_icon)

	local play = display.newText( {
		text = R.strings('continue_play'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 25,
		y = screenH - 90,
	} )
	play.anchorX = 0
	play:setFillColor( 1, 1, 1 )
	pause_group:insert(play)

	local play_button = display.newRect( halfW + 25, screenH - 90, 180, 40 )
	play_button.alpha = 0
	play_button.isHitTestable = true
	pause_group:insert( play_button )

	utils.handlerAdd(play_button, 'touch', continue_handler)

	-- main menu
	local menu_icon = display.newImageRect( sceneGroup, 'images/menu.png', 16, 16 )
	menu_icon.anchorX = 1
	menu_icon.x = halfW - 40
	menu_icon.y = screenH - 50
	pause_group:insert(menu_icon)

	local menu = display.newText( {
		text = R.strings('main_menu'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 25,
		y = screenH - 50,
	} )
	menu.anchorX = 0
	menu:setFillColor( 1, 1, 1 )
	pause_group:insert(menu)

	local menu_button = display.newRect( halfW + 15, screenH - 50, 160, 40 )
	menu_button.alpha = 0
	menu_button.isHitTestable = true
	pause_group:insert( menu_button )

	utils.handlerAdd(menu_button, 'touch', main_menu_handler)
end

function build_time_out_screen(sceneGroup)
	-- pause grupa
	time_out_group = display.newGroup()
	time_out_group.alpha = 0
	sceneGroup:insert( time_out_group )

	local box = display.newRect( 0, 0, screenW, screenH )
	box.anchorX = 0
	box.anchorY = 0
	box.alpha = 0.85
	box:setFillColor( 0, 0, 0 )
	time_out_group:insert( box )

	utils.handlerAdd(box, 'touch', function ( ... )
		return true
	end)

	local solved_label = display.newText( {
		text = R.strings('time_out'),
		fontSize = 48,
		font = 'Ubuntu',
		x = halfW,
		y = halfH - 50,
	} )
	solved_label:setFillColor( 1, 1, 1 )
	time_out_group:insert(solved_label)

	-- replay
	local menu_icon = display.newImageRect( sceneGroup, 'images/replay.png', 16, 15 )
	menu_icon.anchorX = 1
	menu_icon.x = halfW - 40
	menu_icon.y = screenH - 90
	time_out_group:insert(menu_icon)

	local menu = display.newText( {
		text = R.strings('replay'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 25,
		y = screenH - 90,
	} )
	menu.anchorX = 0
	menu:setFillColor( 1, 1, 1 )
	time_out_group:insert(menu)

	local menu_button = display.newRect( halfW + 15, screenH - 90, 160, 40 )
	menu_button.alpha = 0
	menu_button.isHitTestable = true
	time_out_group:insert( menu_button )

	utils.handlerAdd(menu_button, 'touch', replay_handler)

	-- main menu
	local menu_icon = display.newImageRect( sceneGroup, 'images/menu.png', 16, 16 )
	menu_icon.anchorX = 1
	menu_icon.x = halfW - 40
	menu_icon.y = screenH - 50
	time_out_group:insert(menu_icon)

	local menu = display.newText( {
		text = R.strings('main_menu'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW - 25,
		y = screenH - 50,
	} )
	menu.anchorX = 0
	menu:setFillColor( 1, 1, 1 )
	time_out_group:insert(menu)

	local menu_button = display.newRect( halfW + 15, screenH - 50, 160, 40 )
	menu_button.alpha = 0
	menu_button.isHitTestable = true
	time_out_group:insert( menu_button )

	utils.handlerAdd(menu_button, 'touch', main_menu_handler)
end

function build_delay_screen(sceneGroup)
	delay_box = display.newRect( 0, 0, screenW, screenH )
	delay_box.anchorX = 0
	delay_box.anchorY = 0
	delay_box.alpha = 0
	delay_box.isHitTestable = false
	delay_box:setFillColor( 1, 1, 1 )
	sceneGroup:insert( delay_box )

	utils.handlerAdd(delay_box, 'touch', function ( ... )
		return true
	end)
end

function time_out()
	if settings.music then
		audio.fadeOut( {channel = sounds.channel.MUSIC_CHANNEL, time = 500} )
	end

	sounds.play(sounds.sound.time_out, sounds.channel.SFX_CHANNEL)
	sounds.vibrate()

	stop_timer()

	-- zobrazime screen
	time_out_group.alpha = 1

	if ads.isLoaded("interstitial") then
		ads.show( "interstitial", { x=0, y=0, testMode = false } )
	end

	-- a do analytics posleme event
	ga:event('game', 'time_out', 'difficulty', selected_difficulty)
	ga:event('game', 'time_out', 'time', time)
end

function time_timer_handler( ... )
	if game_type == 'time-challenge' then
		time = time - 1
	else
		time = time + 1
	end

	update_hid()

	-- ak vyprsal cas
	if game_type == 'time-challenge' and time == 0 then
		time_out()
	end
end

function stop_timer( ... )
	--utils.debug('game:stop_timer')

	if time_timer ~= nil then
		timer.cancel( time_timer )
		time_timer = nil
	end
end

function scene:create( event )
	local sceneGroup = self.view

	-- time
	local time_icon = display.newImageRect( sceneGroup, 'images/time.png', 25, 28 )
	time_icon.anchorX = 0
	time_icon.x = 15
	time_icon.y = 30

	time_text = display.newText( {
		text = '0:00',
		fontSize = 18,
		font = 'Ubuntu',
		x = 50,
		y = 30,
	} )
	time_text.anchorX = 0
	time_text:setFillColor( 1, 1, 1 )
	sceneGroup:insert(time_text)

	-- moves
	local moves_icon = display.newImageRect( sceneGroup, 'images/moves.png', 23, 23 )
	moves_icon.anchorX = 0
	moves_icon.x = 105
	moves_icon.y = 30

	moves_text = display.newText( {
		text = '0',
		fontSize = 18,
		font = 'Ubuntu',
		x = 135,
		y = 30,
	} )
	moves_text.anchorX = 0
	moves_text:setFillColor( 1, 1, 1 )
	sceneGroup:insert(moves_text)

	-- level
	local level_label = display.newText( {
		text = R.strings('level'),
		fontSize = 18,
		font = 'Ubuntu',
		x = screenW - 45,
		y = 30,
	} )
	level_label.anchorX = 1
	level_label:setFillColor( 1, 1, 1 )
	sceneGroup:insert(level_label)

	level_text = display.newText( {
		text = '',
		fontSize = 18,
		font = 'Ubuntu',
		x = screenW - 40,
		y = 30,
	} )
	level_text.anchorX = 0
	level_text:setFillColor( 1, 1, 1 )
	sceneGroup:insert(level_text)

	-- pause
	local pause_icon = display.newImageRect( sceneGroup, 'images/pause.png', 16, 16 )
	pause_icon.anchorX = 0
	pause_icon.x = 15
	pause_icon.y = screenH - 30
	sceneGroup:insert(pause_icon)

	local pause = display.newText( {
		text = R.strings('pause'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 40,
		y = screenH - 30,
	} )
	pause.anchorX = 0
	pause:setFillColor( 1, 1, 1 )
	sceneGroup:insert(pause)

	local pause_button = display.newRect( 60, screenH - 30, 120, 40 )
	pause_button.alpha = 0
	pause_button.isHitTestable = true
	sceneGroup:insert( pause_button )

	utils.handlerAdd(pause_button, 'touch', pause_handler)

	-- show numbers
	local numbers = display.newText( {
		text = R.strings('numbers'),
		fontSize = 18,
		font = 'Ubuntu',
	} )
	numbers.anchorX = 0
	numbers.x = screenW - numbers.width - 14
	numbers.y = screenH - 30
	numbers:setFillColor( 1, 1, 1 )
	sceneGroup:insert(numbers)

	local numbers_icon = display.newImageRect( sceneGroup, 'images/numbers.png', 16, 16 )
	numbers_icon.anchorX = 0
	numbers_icon.x = screenW - numbers.width - 38
	numbers_icon.y = screenH - 30
	sceneGroup:insert(numbers_icon)

	local numbers_button = display.newRect( screenW - 60, screenH - 30, 120, 40 )
	numbers_button.alpha = 0
	numbers_button.isHitTestable = true
	sceneGroup:insert( numbers_button )

	utils.handlerAdd(numbers_button, 'touch', show_numbers_handler)

	level = 1
	game_type = nil

	image_path = nil
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- ak sa loauje reklama z predosleho requestu, tak ju zrusime
		ads.hide()

		cols = settings.selected_difficulty + 2
		rows = settings.selected_difficulty + 2

		total_segments = cols * rows

		show_help_numbers = false

		matrix_col_size = 286 / cols
		matrix_row_size = 286 / rows

		if event.params then 
			-- init levelu
			if event.params.level then level = event.params.level end

			-- ak je zadefinovany typ hry
			if event.params.game_type then
				game_type = event.params.game_type
			end

			if game_type == 'random-puzzle' then
				-- zabezpecime nulovanie ulozenej cesty
				image_path = nil
			end
		end

		-- moves resetneme na 0
		moves = 0

		-- time nastavime podla typu hry
		if game_type == 'time-challenge' then
			-- obmedzeny cas
			time = R.arrays('category_time_bonus')[settings.selected_difficulty]
		else
			-- standardne pocitame cas
			time = 0
		end

		-- hracia plocha
		build_playground(sceneGroup)

		build_pause_screen(sceneGroup)

		build_result_screen(sceneGroup)

		build_time_out_screen(sceneGroup)

		build_delay_screen(sceneGroup)

		shuffle()

		sounds.vibrate()

		-- aby sa zobrazili spravne default hodnoty
		update_hid()
		
		-- spustime timer
		time_timer = timer.performWithDelay( 1000, time_timer_handler, 0 )
	elseif phase == "did" then
		-- load reklamy
		ads.load( "interstitial", { testMode=false } )
		
		if settings.music then
			audio.play( sounds.sound.music, {channel = sounds.channel.MUSIC_CHANNEL, loops = -1} )
			audio.setVolume( 0.20, {channel = sounds.channel.MUSIC_CHANNEL} )
		end

		settings.game_play_counter = settings.game_play_counter + 1
		utils.saveSettings(settings)

		--utils.debug('game:show', game_type)

		ga:view('game')
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		
	elseif phase == "did" then
		pause_group:removeSelf( )
		result_group:removeSelf( )
		time_out_group:removeSelf( )

		remove_playground()

		if settings.music then
			audio.fadeOut( {channel = sounds.channel.MUSIC_CHANNEL, time = 500} )
		end
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene