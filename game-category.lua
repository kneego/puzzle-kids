-----------------------------------------------------------------------------------------
--
-- settings.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-----------------------------------------------------------------------------------------

local category_preview_items

local difficulty
local game_type

-----------------------------------------------------------------------------------------

function show_category_preview()
	for i = 1, #category_preview_items do
		category_preview_items[i].alpha = 0
	end

	category_preview_items[settings.selected_category].alpha = 1
end

function previous_category_touch_handler( self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	local selected_category = settings.selected_category - 1
	
	if selected_category < 1 then selected_category = #category_preview_items end

	settings.selected_category = selected_category
	utils.saveSettings(settings)

	show_category_preview()

	return true
end

function next_category_touch_handler( self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	local selected_category = settings.selected_category + 1
	
	if selected_category > #category_preview_items then selected_category = 1 end

	settings.selected_category = selected_category
	utils.saveSettings(settings)

	show_category_preview()

	return true
end

function show_difficulty()
	for i = 1, #difficulty do
		difficulty[i].alpha = 0.5
	end

	difficulty[settings.selected_difficulty].alpha = 1
end

function difficulty_touch_handler( self, event )
	if event.phase ~= 'began' then return true end

	sounds.play(sounds.sound.button_click)

	settings.selected_difficulty = self.difficulty

	utils.saveSettings(settings)

	show_difficulty()

	return true
end

function scene:create( event )
	sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW * display_ar, screenH )
	background.x, background.y = halfW, halfH
	sceneGroup:insert( background )

	-- top background
	local top_bg = display.newRect( 0, 0, screenW, 57 )
	top_bg.anchorX = 0
	top_bg.anchorY = 0
	top_bg:setFillColor( utils.color(241, 196, 15) )
	sceneGroup:insert(top_bg)

	-- back button
	local back = display.newImageRect( 'images/left.png', 18, 18 )
	back.anchorX = 0
	back.x = 20
	back.y = 30
	sceneGroup:insert( back )

	local back_button = display.newRect( 0, 0, 57, 57 )
	back_button.anchorX = 0
	back_button.anchorY = 0
	back_button.alpha = 0
	back_button.isHitTestable = true
	sceneGroup:insert( back_button )

	utils.handlerAdd(back_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'menu' )
		end

		return true
	end )

	-- label settings
	local labelSettings = display.newText( {
		text = R.strings('select_category'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 60,
		y = 30
	} )
	labelSettings.anchorX = 0
	labelSettings:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelSettings )

	-- category preview
	local image_border = display.newRect( halfW, 200, 200, 200 )
	image_border:setStrokeColor( utils.color(241, 196, 15) )
	image_border.strokeWidth = 1
	image_border.alpha = 0.3
	sceneGroup:insert(image_border)

	category_preview_items = {}
	for i = 1, #R.arrays('category_preview') do
		local _group = display.newGroup( )
		_group.alpha = 0

		sceneGroup:insert(_group)

		category_preview_items[i] = _group

		local _item = display.newImageRect( R.arrays('category_preview')[i], 200, 200 )
		_item.x = halfW
		_item.y = 200
		_item.alpha = 1
		_group:insert( _item )

		local _description = display.newEmbossedText( {
			text = R.arrays('category_description')[i],
			fontSize = 18,
			font = 'Ubuntu',
			x = halfW,
			y = 200,
			width = 180,
			height = 0,
			align = 'center',
		} )
		_description.alpha = 1
		_description:setFillColor( 1, 1, 1 )
		_group:insert( _description )
	end

	-- prev and next button
	local previous_category = display.newImageRect( 'images/left.png', 22, 22 )
	previous_category.anchorX = 0
	previous_category.x = 20
	previous_category.y = 198
	sceneGroup:insert(previous_category)

	local previous_category_button = display.newRect( 25, 198, 50, 50 )
	previous_category_button.alpha = 0
	previous_category_button.isHitTestable = true
	sceneGroup:insert( previous_category_button )

	utils.handlerAdd(previous_category_button, 'touch', previous_category_touch_handler)

	local next_category = display.newImageRect( 'images/right.png', 22, 22 )
	next_category.anchorX = 1
	next_category.x = screenW - 20
	next_category.y = 198
	sceneGroup:insert(next_category)

	local next_category_button = display.newRect( screenW - 25, 198, 50, 50 )
	next_category_button.alpha = 0
	next_category_button.isHitTestable = true
	sceneGroup:insert( next_category_button )

	utils.handlerAdd(next_category_button, 'touch', next_category_touch_handler)

	-- separator
	local separator = display.newLine( sceneGroup, 0, screenH - 70, screenW, screenH - 70 )
	separator:setStrokeColor( 1, 1, 1, 0.3 )
	separator.strokeWidth = 1

	-- play button
	local play = display.newImageRect( 'images/button-bg.png', 290, 42 )
	play.x = halfW
	play.y = screenH - 36
	sceneGroup:insert(play)

	utils.handlerAdd(play, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'game-level', {
				params = {
					game_type = game_type
				}
			} )
		end

		return true
	end)

	local play_label = display.newText( {
		text = R.strings('play'),
		fontSize = 18,
		font = 'Ubuntu',
		x = halfW,
		y = screenH - 36
	} )
	play_label:setFillColor( 1, 1, 1 )
	sceneGroup:insert( play_label )

	-- narocnost hry
	difficulty = {}

	local difficulty_configuration = {
		{img = 'images/3x3.png', x = halfW - 60},
		{img = 'images/4x4.png', x = halfW},
		{img = 'images/5x5.png', x = halfW + 60},
	}

	for i = 1, #difficulty_configuration do
		local _item = display.newImageRect( difficulty_configuration[i].img, 50, 50 )
		_item.x = difficulty_configuration[i].x
		_item.y = 345
		_item.alpha = 0.5
		_item.difficulty = i
		sceneGroup:insert(_item)
		utils.handlerAdd(_item, 'touch', difficulty_touch_handler)

		difficulty[i] = _item
	end
end

function scene:show( event )
	local phase = event.phase
	
	if phase == "will" then
		if event.params then
			if event.params.game_type then
				game_type = event.params.game_type
			end
		end
	elseif phase == "did" then
		show_category_preview()
		show_difficulty()

		ga:view('game-category')
	end	
end

function scene:hide( event )
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	category_preview_items = nil
	difficulty = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene