# Puzzle Kids

Full source code and assets of Puzzle Kids game for [Android](https://play.google.com/store/apps/details?id=org.kneego.puzzle_kids) and [iOS](https://itunes.apple.com/us/app/kids-slide-puzzle/id955064625?mt=8).

The game uses the [Corona SDK](https://coronalabs.com) game engine.
