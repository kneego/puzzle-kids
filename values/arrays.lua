local _M = {
	default = {},
	sk = {},
	cs = {},
}

_M.default.category_description = {
	'ANIMAL',
	'CAR',
	'FRUIT',
	'DOMESTICATED ANIMAL',
	'VEGETABLE',
}

_M.sk.category_description = {
	'ZVIERATKÁ',
	'AUTÍČKA',
	'OVOCIE',
	'DOMÁCE ZVIERATKÁ',
	'ZELENINA',
}

_M.cs.category_description = {
	'ZVÍŘÁTKA',
	'AUTÍČKA',
	'OVOCE',
	'DOMÁCÍ ZVÍŘÁTKA',
	'ZELENINA',
}

_M.default.category_label = {
	'animal',
	'car',
	'fruit',
	'home_animal',
	'vegetable',
}

_M.default.category_time_bonus = {
	30,  -- 3x3
	60,  -- 4x4
	120,  -- 5x5
}

_M.default.category_preview = {
	'images/preview/animal.jpg',
	'images/preview/car.jpg',
	'images/preview/fruit.jpg',
	'images/preview/home-animal.jpg',
	'images/preview/vegetable.jpg',
}

_M.default.puzzle_images = {
	animal = {
		'hroch-01.jpg',
		'lev-03.jpg',
		'nosorozec-04.jpg',
		'opica-02.jpg',
		'tiger-01.jpg',
		'zirafa-05.png'
	},
	car = {
		'dievca-auto-03.jpg',
		'nakladak-03.jpg',
		'osobne-03.jpg',
		'police-03.jpg',
		'sportove-03.jpg',
		'veteran-03.png'
	},
	fruit = {
		'bananik-01.jpg',
		'citronik-01.jpg',
		'hrozienko-01.jpg',
		'hruska-01.jpg',
		'jablcko-01.jpg',
		'pomaranc-01.jpg',
	},
	home_animal = {
		'cicamica-01.jpg',
		'havko-03.jpg',
		'konik-02-02.jpg',
		'krajinka-03.jpg',
		'prasiatko-02.jpg',
		'sliepocka-01.png'
	},
	vegetable = {
		'cesnacik-01.jpg',
		'cibulka-01.jpg',
		'mrkvicka-01.jpg',
		'paprika-01.jpg',
		'paradajka-01.jpg',
		'uhorka-01.png'
	}
}

_M.default.admob_id = {
	['iPhone OS'] = 'ca-app-pub-1210274935500874/8126875541',
	['Android'] = 'ca-app-pub-1210274935500874/5173409147'
}

_M.default.board_id = {
	['iPhone OS'] = {
		'3x3',
		'4x4',
		'5x5',
	},
	['Android'] = {
		'CgkIobuepugCEAIQAA', -- 3x3
		'CgkIobuepugCEAIQAQ', -- 4x4
		'CgkIobuepugCEAIQAg', -- 5x5
	}
}

_M.default.achievements_play = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{ -- 3x3 - posielam vsetky tieto achievements (10x, 50x, 100x)
			'CgkIobuepugCEAIQAw', 'CgkIobuepugCEAIQBA', 'CgkIobuepugCEAIQBQ',
		},
		{ -- 4x4 - posielam vsetky tieto achievements (10x, 50x, 100x)
			'CgkIobuepugCEAIQBg', 'CgkIobuepugCEAIQBw', 'CgkIobuepugCEAIQCA',
		},
		{ -- 5x5 - posielam vsetky tieto achievements (10x, 50x, 100x)
			'CgkIobuepugCEAIQCQ', 'CgkIobuepugCEAIQCg', 'CgkIobuepugCEAIQCw',
		},
	}
}

_M.default.achievements_play_three_stars = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIobuepugCEAIQDA', 'CgkIobuepugCEAIQDQ', 'CgkIobuepugCEAIQDg'},
		{'CgkIobuepugCEAIQDw', 'CgkIobuepugCEAIQEA', 'CgkIobuepugCEAIQEQ'},
		{'CgkIobuepugCEAIQEg', 'CgkIobuepugCEAIQEw', 'CgkIobuepugCEAIQFA'},
	}
}

_M.default.achievements_five_minutes = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIobuepugCEAIQFQ', 'CgkIobuepugCEAIQFg', 'CgkIobuepugCEAIQFw'},
		{'CgkIobuepugCEAIQGA', 'CgkIobuepugCEAIQGQ', 'CgkIobuepugCEAIQGg'},
		{'CgkIobuepugCEAIQGw', 'CgkIobuepugCEAIQHA', 'CgkIobuepugCEAIQHQ'},
	}
}

_M.default.achievements_one_minute = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIobuepugCEAIQHg', 'CgkIobuepugCEAIQHw', 'CgkIobuepugCEAIQIA'},
		{'CgkIobuepugCEAIQIQ', 'CgkIobuepugCEAIQIg', 'CgkIobuepugCEAIQIw'},
		{nil, nil, nil},
	}
}

return _M