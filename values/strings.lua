local _M = {}

_M.default = {
	-- menu
	game_name_1 = 'Kids',
	game_name_2 = 'Puzzle',

	classic_play = 'CLASSIC PUZZLE',
	time_challenge = 'TIME CHALLENGE',
	random_puzzle = 'RANDOM PUZZLE',
	leaderboard = 'LEADERBOARD',
	settings = 'SETTINGS',

	-- settings
	settings_head = 'SETTINGS',
	sounds = 'SOUNDS',
	music = 'MUSIC',
	vibrations = 'VIBRATIONS',

	-- select category
	select_category = 'SELECT CATEGORY',
	play = 'PLAY',

	-- select level
	select_level_head = 'SELECT LEVEL',

	-- game
	pause = 'PAUSE',
	level = 'LEVEL',
	numbers = 'NUMBERS',
	paused = 'PAUSED',
	continue_play = 'CONTINUE',
	main_menu = 'MAIN MENU',
	solved = 'Amazing',
	good_job = 'Puzzle solved!',
	score = 'SCORE',
	play_next_level = 'NEXT PUZZLE',
	replay = 'REPLAY',
	time_out = 'TIME OUT',
}

_M.cs = {
	-- menu
	game_name_1 = 'Puzzle',
	game_name_2 = 'pro děti',

	classic_play = 'KLASICKÁ HRA',
	time_challenge = 'ČASOVÁ VÝZVA',
	random_puzzle = 'NÁHODNÉ PUZZLE',
	leaderboard = 'NEJLEPŠÍ HRÁČI',
	settings = 'NASTAVENÍ',

	-- settings
	settings_head = 'NASTAVENÍ',
	sounds = 'ZVUKY',
	music = 'HUDBA',
	vibrations = 'VIBRACE',

	-- select category
	select_category = 'VOLBA KATEGORIE',
	play = 'HRAJ',

	-- select level
	select_level_head = 'VOLBA ÚROVNĚ',

	-- game
	pause = 'PAUZA',
	level = 'ÚROVEŇ',
	numbers = 'ČÍSLA',
	paused = 'PAUZA',
	continue_play = 'POKRAČUJ',
	main_menu = 'HLAVNÉ MENU',
	solved = 'Paráda',
	good_job = 'Hádanka vyřešena!',
	score = 'SCORE',
	play_next_level = 'DALŠÍ HRA',
	replay = 'OPAKUJ',
	time_out = 'ČAS VYPRŠEL',
}


_M.sk = {
	-- menu
	game_name_1 = 'Puzzle',
	game_name_2 = 'pre deti',

	classic_play = 'KLASICKÁ HRA',
	time_challenge = 'ČASOVÁ VÝZVA',
	random_puzzle = 'NÁHODNÉ PUZZLE',
	leaderboard = 'NAJLEPŠÍ HRÁČI',
	settings = 'NASTAVENIA',

	-- settings
	settings_head = 'NASTAVENIA',
	sounds = 'ZVUKY',
	music = 'HUDBA',
	vibrations = 'VIBRÁCIE',

	-- select category
	select_category = 'VOĽBA KATEGÓRIE',
	play = 'HRAJ',

	-- select level
	select_level_head = 'VOĽBA ÚROVNE',

	-- game
	pause = 'PAUZA',
	level = 'ÚROVEŇ',
	numbers = 'ČÍSLA',
	paused = 'PAUZA',
	continue_play = 'POKRAČUJ',
	main_menu = 'HLAVNÉ MENU',
	solved = 'Paráda',
	good_job = 'Hádanka vyriešená!',
	score = 'SCORE',
	play_next_level = 'ĎALŠIA HRA',
	replay = 'OPAKUJ',
	time_out = 'ČAS VYPRŠAL',
}

return _M