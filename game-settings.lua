-----------------------------------------------------------------------------------------
--
-- settings.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local music
local sounds_switch
local vibrations
local sceneGroup

function sounds_button_click( event )
	if settings.sounds then
		settings.sounds = false
	else
		settings.sounds = true
	end

	utils.saveSettings( settings )

	show_sounds_button()

	sounds.play(sounds.sound.button_click)

	ga:event('game-settings', 'button', 'sounds_button_click', settings.sounds)
end

function vibrations_button_click( event )
	if settings.vibrations then
		settings.vibrations = false
	else
		settings.vibrations = true
	end

	utils.saveSettings( settings )

	show_vibrations_button()

	sounds.play(sounds.sound.button_click)

	ga:event('game-settings', 'button', 'vibrations_button_click', settings.vibrations)
end

function music_button_click( event )
	if settings.music then
		settings.music = false
	else
		settings.music = true
	end

	utils.saveSettings( settings )

	show_music_button()

	sounds.play(sounds.sound.button_click)

	ga:event('game-settings', 'button', 'music_button_click', settings.music)
end

function show_sounds_button()
	-- remove old image
	if sounds_switch ~= nil then sounds_switch:removeSelf( ); sounds_switch = nil end

	local sounds_image
	if settings.sounds then sounds_image = 'images/on.png' else sounds_image = 'images/off.png' end

	sounds_switch = display.newImageRect( sounds_image, 30, 16 )
	sounds_switch.anchorX = 1
	sounds_switch.x = screenW - 35
	sounds_switch.y = 90
end

function show_music_button()
	-- utils.debug('game-settings:show_vibrations_button')

	-- remove old image
	if music ~= nil then music:removeSelf( ); music = nil end

	local music_image
	if settings.music then music_image = 'images/on.png' else music_image = 'images/off.png' end

	music = display.newImageRect( music_image, 30, 16 )
	music.anchorX = 1
	music.x = screenW - 35
	music.y = 153
end

function show_vibrations_button()
	-- utils.debug('game-settings:show_vibrations_button')

	-- remove old image
	if vibrations ~= nil then vibrations:removeSelf( ); vibrations = nil end

	local vibrations_image
	if settings.vibrations then vibrations_image = 'images/on.png' else vibrations_image = 'images/off.png' end

	vibrations = display.newImageRect( vibrations_image, 30, 16 )
	vibrations.anchorX = 1
	vibrations.x = screenW - 35
	vibrations.y = 218
end

function scene:create( event )
	sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW * display_ar, screenH )
	background.x, background.y = halfW, halfH
	sceneGroup:insert( background )

	-- top background
	local top_bg = display.newRect( 0, 0, screenW, 57 )
	top_bg.anchorX = 0
	top_bg.anchorY = 0
	top_bg:setFillColor( utils.color(241, 196, 15) )
	sceneGroup:insert(top_bg)

	-- back button
	local back = display.newImageRect( 'images/left.png', 18, 18 )
	back.anchorX = 0
	back.x = 20
	back.y = 30
	sceneGroup:insert( back )

	local back_button = display.newRect( 0, 0, 57, 57 )
	back_button.anchorX = 0
	back_button.anchorY = 0
	back_button.alpha = 0
	back_button.isHitTestable = true
	sceneGroup:insert( back_button )

	utils.handlerAdd(back_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'menu' )
		end

		return true
	end )

	-- label settings
	local labelSettings = display.newText( {
		text = R.strings('settings_head'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 54,
		y = 30
	} )
	labelSettings.anchorX = 0
	labelSettings:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelSettings )

	-- sounds
	local labelSounds = display.newText( {
		text = R.strings('sounds'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 25,
		y = 90
	} )
	labelSounds.anchorX = 0
	labelSounds:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelSounds )

	utils.handlerAdd(labelSounds, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds_button_click()
		end

		return true
	end )

	-- music
	local labelMusic = display.newText( {
		text = R.strings('music'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 25,
		y = 153
	} )
	labelMusic.anchorX = 0
	labelMusic:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelMusic )

	utils.handlerAdd(labelMusic, 'touch', function ( self, event )
		if event.phase == 'began' then
			music_button_click()
		end

		return true
	end )

	-- vibrations
	local labelVibrations = display.newText( {
		text = R.strings('vibrations'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 25,
		y = 218
	} )
	labelVibrations.anchorX = 0
	labelVibrations:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelVibrations )

	utils.handlerAdd(labelVibrations, 'touch', function ( self, event )
		if event.phase == 'began' then
			vibrations_button_click()
		end

		return true
	end )

	-- separators
	local separator_position = {120, 183, 248}
	for i = 1, 3 do
		local _separator = display.newLine( sceneGroup, 0, separator_position[i], screenW, separator_position[i] )
		_separator:setStrokeColor( 1, 1, 1, 0.3 )
		_separator.strokeWidth = 1
	end

	-- vibration button
	local vibrations_button = display.newRect( screenW - 50, 218, 60, 40 )
	vibrations_button.alpha = 0
	vibrations_button.isHitTestable = true
	sceneGroup:insert( vibrations_button )

	utils.handlerAdd(vibrations_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			vibrations_button_click()
		end

		return true
	end )

	-- music button
	local music_button = display.newRect( screenW - 50, 153, 60, 40 )
	music_button.alpha = 0
	music_button.isHitTestable = true
	sceneGroup:insert( music_button )

	utils.handlerAdd(music_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			music_button_click()
		end

		return true
	end )

	-- sounds button
	local sounds_button = display.newRect( screenW - 50, 90, 60, 40 )
	sounds_button.alpha = 0
	sounds_button.isHitTestable = true
	sceneGroup:insert( sounds_button )

	utils.handlerAdd(sounds_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds_button_click()
		end

		return true
	end )
end

function scene:show( event )
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		show_music_button()
		show_vibrations_button()
		show_sounds_button()

		ga:view('settings')
	end	
end

function scene:hide( event )
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		if music ~= nil then music:removeSelf( ); music = nil end
		if sounds_switch ~= nil then sounds_switch:removeSelf( ); sounds_switch = nil end
		if vibrations ~= nil then vibrations:removeSelf( ); vibrations = nil end
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene