-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local system = require( 'system' )
local native = require('native')

function scene:create( event )
	local sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW * display_ar, screenH )
	background.x, background.y = halfW, halfH
	sceneGroup:insert( background )

	-- logo
	local matrix = display.newImageRect( 'images/logo-matrix.png', 60, 60 )
	matrix.x = halfW - 100
	matrix.y = 50
	sceneGroup:insert( matrix )

	local game_name_1 = display.newText( {
		text = R.strings('game_name_1'),
		fontSize = 28,
		font = 'Ubuntu',
		x = halfW - 58,
		y = 35
	} )
	game_name_1.anchorX = 0
	game_name_1:setFillColor( 1, 1, 1 )
	sceneGroup:insert( game_name_1 )

	local game_name_2 = display.newText( {
		text = R.strings('game_name_2'),
		fontSize = 28,
		font = 'Ubuntu',
		x = halfW - 58,
		y = 65
	} )
	game_name_2.anchorX = 0
	game_name_2:setFillColor( 1, 1, 1 )
	sceneGroup:insert( game_name_2 )

	-- classic play
	local classic_play = display.newImageRect( 'images/button-bg.png', 211, 53 )
	classic_play.x = screenW * 0.5
	classic_play.y = halfH - 100
	sceneGroup:insert( classic_play )

	utils.handlerAdd(classic_play, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'game-category', {
				params = {
					game_type = 'classic-play',
				}
			} )
		end

		return true
	end )

	local label_classic_play = display.newText( {
		text = R.strings('classic_play'),
		fontSize = 18,
		font = 'Ubuntu',
		x = screenW * 0.5,
		y = halfH - 100
	} )
	label_classic_play:setFillColor( 1, 1, 1 )
	sceneGroup:insert( label_classic_play )

	-- time challenge
	local time_challenge = display.newImageRect( 'images/button-bg.png', 211, 53 )
	time_challenge.x = screenW * 0.5
	time_challenge.y = halfH - 40
	sceneGroup:insert( time_challenge )

	utils.handlerAdd(time_challenge, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'game-category', {
				params = {
					game_type = 'time-challenge',
				}
			} )
		end

		return true
	end )

	local label_time_challenge = display.newText( {
		text = R.strings('time_challenge'),
		fontSize = 18,
		font = 'Ubuntu',
		x = screenW * 0.5,
		y = halfH - 40
	} )
	label_time_challenge:setFillColor( 1, 1, 1 )
	sceneGroup:insert( label_time_challenge )

	-- random puzzle
	local random_puzlle = display.newImageRect( 'images/button-bg.png', 211, 53 )
	random_puzlle.x = screenW * 0.5
	random_puzlle.y = halfH + 20
	sceneGroup:insert( random_puzlle )

	utils.handlerAdd(random_puzlle, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'game', {
				params = {
					game_type = 'random-puzzle',
				}
			} )
		end

		return true
	end )

	local label_random_puzlle = display.newText( {
		text = R.strings('random_puzzle'),
		fontSize = 18,
		font = 'Ubuntu',
		x = screenW * 0.5,
		y = halfH + 20
	} )
	label_random_puzlle:setFillColor( 1, 1, 1 )
	sceneGroup:insert( label_random_puzlle )

	-- leaderboards
	local leaderboards = display.newImageRect( 'images/leaderboards.png', 18, 24 )
	leaderboards.anchorX = 0
	leaderboards.x = 10
	leaderboards.y = screenH - 25
	sceneGroup:insert( leaderboards )

	local labelLeaderboards = display.newText( {
		text = R.strings('leaderboard'),
		fontSize = 12,
		font = 'Ubuntu',
		x = 35,
		y = screenH - 25
	} )
	labelLeaderboards.anchorX = 0
	labelLeaderboards:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelLeaderboards )

	local leaderboards_button = display.newRect( 60, screenH - 25, 120, 50 )
	leaderboards_button.alpha = 0
	leaderboards_button.isHitTestable = true
	sceneGroup:insert( leaderboards_button )

	utils.handlerAdd(leaderboards_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			game_network.show_leaderboards()
		end
	end )

	-- settings
	local labelSettings = display.newText( {
		text = R.strings('settings'),
		fontSize = 12,
		font = 'Ubuntu',
		x = screenW - 15,
		y = screenH - 25
	} )
	labelSettings.anchorX = 1
	labelSettings:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelSettings )

	local settings = display.newImageRect( 'images/settings.png', 18, 18 )
	settings.anchorX = 1
	settings.x = screenW - labelSettings.width - 24
	settings.y = screenH - 25
	sceneGroup:insert( settings )

	local settings_button = display.newRect( screenW - 50, screenH - 25, 100, 50 )
	settings_button.alpha = 0
	settings_button.isHitTestable = true
	sceneGroup:insert( settings_button )

	utils.handlerAdd(settings_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'game-settings' )
		end
	end )

	-- facebook
	-- local fb = display.newImageRect( 'images/fb.png', 67, 70 )
	-- fb.anchorX = 1
	-- fb.anchorY = 0
	-- fb.x = screenW - 5
	-- fb.y = 0
	-- sceneGroup:insert( fb )

	-- utils.handlerAdd(fb, 'touch', function ( self, event )
	-- 	if event.phase == 'began' then
	-- 		system.openURL( R.strings('facebook_url') )
	-- 	end
	-- end )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		
		-- inicializujeme game network hned pri spusteni
		game_network.init()

		ga:view('menu')
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene