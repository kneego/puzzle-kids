-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

require( 'knee.globals' )

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- data = require( "data" )
ads = require( 'ads' )
utils = require( 'knee.utils' )
back_button = require( 'knee.back_button' )

game_network = require( 'knee.game_network' )
game_network.debugging = false

-- resources (strings, arrays)
R = require( 'resources' )
sounds = require('sounds')

ga = require( 'knee.ga' )
ga:init( 'UA-39791763-14' )

-- reklamu zobraujeme inu pre iOS a inu pre Android
ads.init( 'admob', R.arrays('admob_id')[platform_name])

back_button.init()

debugging = true

-- load settings
settings = utils.loadSettings({
	-- default empty data
	emptyData = {
		vibrations = true,
		sounds = true,
		music = true,
		game_play_counter = 0,
		selected_category = 1,
		selected_difficulty = 2
	}
})

-- inicializacia levelov podla definicie obrazkov
if settings['levels'] == nil then settings['levels'] = {} end

for i = 1, #R.arrays('category_label') do
	local _category = R.arrays('category_label')[i]
	if settings['levels'][_category] == nil then
		-- nova kategoria
		settings['levels'][_category] = {
			unlocked = 1,
			level_count = #R.arrays('puzzle_images')[_category],
			stars = {}
		}
	end

	for j = 1, #R.arrays('puzzle_images')[_category] do
		-- nastavime pocet hviezdiciek
		if settings['levels'][_category]['stars'][j] == nil then settings['levels'][_category]['stars'][j] = 0 end

		-- odomknuty 1. level
		if j == 1 and settings['levels'][_category]['unlocked'] == nil then settings['levels'][_category]['unlocked'] = 1 end
	end

	utils.saveSettings(settings)
end

-- ulozime definiciu levelov
utils.saveSettings(settings)

local composer = require( 'composer' )
composer.gotoScene( 'menu' )
