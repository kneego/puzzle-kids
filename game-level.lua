-----------------------------------------------------------------------------------------
--
-- settings.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local levelmenu = require('knee.levelmenu')

-----------------------------------------------------------------------------------------

local menu

-----------------------------------------------------------------------------------------

function scene:create( event )
	sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW * display_ar, screenH )
	background.x, background.y = halfW, halfH
	sceneGroup:insert( background )

	-- top background
	local top_bg = display.newRect( 0, 0, screenW, 57 )
	top_bg.anchorX = 0
	top_bg.anchorY = 0
	top_bg:setFillColor( utils.color(241, 196, 15) )
	sceneGroup:insert(top_bg)

	-- back button
	local back = display.newImageRect( 'images/left.png', 18, 18 )
	back.anchorX = 0
	back.x = 20
	back.y = 30
	sceneGroup:insert( back )

	local back_button = display.newRect( 0, 0, 57, 57 )
	back_button.anchorX = 0
	back_button.anchorY = 0
	back_button.alpha = 0
	back_button.isHitTestable = true
	sceneGroup:insert( back_button )

	utils.handlerAdd(back_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds.play(sounds.sound.button_click)
			composer.gotoScene( 'game-category' )
		end

		return true
	end )

	-- label settings
	local labelSettings = display.newText( {
		text = R.strings('select_level_head'),
		fontSize = 18,
		font = 'Ubuntu',
		x = 60,
		y = 30
	} )
	labelSettings.anchorX = 0
	labelSettings:setFillColor( 1, 1, 1 )
	sceneGroup:insert( labelSettings )
end

function scene:show( event )
	local phase = event.phase
	
	if phase == "will" then
		if event.params then
			if event.params.game_type then
				game_type = event.params.game_type
			end
		end

		-- naplnime zoznam levelov
		local _category_label = R.arrays('category_label')[settings.selected_category]
		local _levels = settings['levels'][_category_label]

		local levels = {}

		for i = 1, #_levels['stars'] do
			levels[i] = {
				name = i,
				stars = _levels['stars'][i],
			}
		end

		menu = levelmenu.createMenu({
			levels = levels,

			left = 0,
			top = (screenH - 300) / 2,
			width = screenW,
			height = 300,

			rows = 4,
			columns = 3,

			paddingLeft = 40,
			paddingRight = 40,
			padding = 0,

			levelCount = 16,

			appendLevelNumber = false,
			drawLevelNameOnLocked = false,
			levelNameOffset = -10,

			itemWidth = 50,
			itemHeight = 53,

			font = 'Ubuntu',
			fontSize = 22,
			fontColor = { utils.color(241, 196, 15) },

			imageLocked = 'images/locked.png',
			imageUnlocked = 'images/unlocked.png',
			imageStar = 'images/star.png',
			imageStarOff = 'images/star-grey.png',
			imageStarWidth = 10,
			imageStarHeight = 10,

			starOffset = 15,
			starSpace = 1,

			unlockedTo = _levels['unlocked'],

			eventListener = function ( event )
				sounds.play(sounds.sound.button_click)

				ga:event('game-level', 'select_level', _category_label, event.target.levelNumber)

				composer.gotoScene( 'game', {
					params = {
						level = event.target.levelNumber,
						game_type = game_type,
					}
				} )

				return true
			end
		})
	elseif phase == "did" then

		ga:view('game-level')
	end	
end

function scene:hide( event )
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		menu:removeSelf( )
		menu = nil
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	category_preview_items = nil
	difficulty = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene